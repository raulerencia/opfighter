﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour
{
    public GameObject pj;
    public int velocity;
    public KeyCode right;
    public KeyCode left;
    public KeyCode jump;
    public Animator animator;
    private bool side = true;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<SpriteRenderer>().transform.Rotate(0, 180, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(right))
        {
            animator.SetBool("right", true);
            if(side == false)
            {
                side = true;
                this.GetComponent<SpriteRenderer>().transform.Rotate(0, 180, 0);
            }
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, 0);
        }
        else if (Input.GetKey(left))
        {
            animator.SetBool("right", true);
            if (side == true)
            {
                side = false;
                this.GetComponent<SpriteRenderer>().transform.Rotate(0, 180, 0);
            }
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, 0);
        }
        else
        {
            animator.SetBool("right", false);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
        

    }
}
