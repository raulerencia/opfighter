﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject pj;
    public int velocity;
    public KeyCode right;
    public KeyCode left;
    public KeyCode jump;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(right)){
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, velocity);
        }
    }
}
